# Mobile App Development Test ( 2 Phase Test )

### Task yang sudah selesai dikerjakan :
* Task 1 -> seluruhnya (terdapat pada branch "Task1_Task2")
* Task 2 -> seluruhnya (terdapat pada branch "Task1_Task2")
* Task 3 -> Change all screen UI with new design (terdapat pada branch Master)

### Task yang belum selesai dikerjakan :
* Task 3 -> add pull to refresh, load the next page when scrolling to bottom & prepare an empty state too. When clicking the maps menu on top right, the event list
view changes into map view.

### Impediment :
* Terdapat tugas kuliah mendadak yang perlu diselesaikan