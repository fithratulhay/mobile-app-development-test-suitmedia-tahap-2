package com.example.suitmediatest;

import android.os.AsyncTask;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.squareup.picasso.Picasso;

public class FetchGuests extends AsyncTask<String, Void, String> {
    private static final String TAG = FetchGuests.class.getSimpleName();
    private ImageView imageView1, imageView2, imageView3, imageView4, imageView5, imageView6;
    public Button button1, button2, button3, button4, button5, button6;
    public String yak;
    public FetchGuests(ImageView imageView1, ImageView imageView2, ImageView imageView3,
                       ImageView imageView4, ImageView imageView5, ImageView imageView6,
                       Button button1, Button button2, Button button3, Button button4,
                       Button button5, Button button6){
        this.imageView1 = imageView1;
        this.imageView2 = imageView2;
        this.imageView3 = imageView3;
        this.imageView4 = imageView4;
        this.imageView5 = imageView5;
        this.imageView6 = imageView6;

        this.button1 = button1;
        this.button2 = button2;
        this.button3 = button3;
        this.button4 = button4;
        this.button5 = button5;
        this.button6 = button6;

        this.yak = "yaa";
    }

    @Override
    protected String doInBackground(String... strings) {
        return NetworkUtils.getGuestInfo(strings[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray guestsArray = jsonObject.getJSONArray("data");

            button1.setText(getName(guestsArray, 0));
            button2.setText(getName(guestsArray, 1));
            button3.setText(getName(guestsArray, 2));
            button4.setText(getName(guestsArray, 3));
            button5.setText(getName(guestsArray, 4));
            button6.setText(getName(guestsArray, 5));

            setImage(guestsArray, imageView1, 0);
            setImage(guestsArray, imageView2, 1);
            setImage(guestsArray, imageView3, 2);
            setImage(guestsArray, imageView4, 3);
            setImage(guestsArray, imageView5, 4);
            setImage(guestsArray, imageView6, 5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getName(JSONArray guestArray, int idx){
        try {
            JSONObject guest = guestArray.getJSONObject(idx);
            String name = guest.getString("first_name") + " " + guest.getString("last_name");
            return name;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void setImage(JSONArray guestArray, ImageView imageView, int idx){
        try {
            JSONObject guest = guestArray.getJSONObject(idx);
            String image = guest.getString("avatar");
            Picasso.get().load(image).into(imageView);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
