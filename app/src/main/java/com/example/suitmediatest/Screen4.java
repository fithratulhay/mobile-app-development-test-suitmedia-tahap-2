package com.example.suitmediatest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Date;

public class Screen4 extends AppCompatActivity {
    private static final String TAG = Screen4.class.getSimpleName();

    private Intent before;

    private ImageView imageView1, imageView2, imageView3, imageView4, imageView5, imageView6;
    private Button button1, button2, button3, button4, button5, button6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen4);

        this.button1 = findViewById(R.id.guestName1);
        this.button2 = findViewById(R.id.guestName2);
        this.button3 = findViewById(R.id.guestName3);
        this.button4 = findViewById(R.id.guestName4);
        this.button5 = findViewById(R.id.guestName5);
        this.button6 = findViewById(R.id.guestName6);

        this.imageView1 = findViewById(R.id.imageView1);
        this.imageView2 = findViewById(R.id.imageView2);
        this.imageView3 = findViewById(R.id.imageView3);
        this.imageView4 = findViewById(R.id.imageView4);
        this.imageView5 = findViewById(R.id.imageView5);
        this.imageView6 = findViewById(R.id.imageView6);

        this.before = getIntent();

        this.setGuest();
    }

    public  void setGuest(){
        //for checking the network state and empty search field case
        Log.d(TAG, "here");
        ConnectivityManager connMngr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMngr.getActiveNetworkInfo();

        if ((networkInfo != null) && networkInfo.isConnected()) {
            FetchGuests test = new FetchGuests(imageView1, imageView2, imageView3, imageView4, imageView5, imageView6,
                    button1, button2, button3, button4, button5, button6);
            test.execute("test");

            Log.d(TAG, test.button1.getText().toString());
        } else {
            Toast.makeText(this, "please check yout network connection and try again", Toast.LENGTH_SHORT).show();
        }
    }

    public void goToScreen2(View view) {
        Intent screen2 = new Intent(this, Screen2.class);

        Button button = findViewById(view.getId());

        screen2.putExtra("guestName", button.getText().toString());
        screen2.putExtra("inputName", before.getStringExtra("inputName"));
        screen2.putExtra("eventName", before.getStringExtra("eventName"));

        startActivity(screen2);
    }

    public Boolean isMonthPrime(){
        Date today = new Date();

        int month = today.getMonth();

        if (month == 1) {
            return  false;
        } else {
            for (int i = 2; i < Math.sqrt(month); i ++){
                if (month % i == 0){
                    return false;
                }
            }

            return true;
        }
    }

    public void goBack(View view) {
        Intent screen2 = new Intent(this, Screen2.class);

        screen2.putExtra("guestName", before.getStringExtra("guestName"));
        screen2.putExtra("inputName", before.getStringExtra("inputName"));
        screen2.putExtra("eventName", before.getStringExtra("eventName"));

        startActivity(screen2);
    }
}
