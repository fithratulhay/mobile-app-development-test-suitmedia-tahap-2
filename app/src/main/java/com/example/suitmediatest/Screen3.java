package com.example.suitmediatest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Screen3 extends AppCompatActivity {
    private static final String TAG = Screen3.class.getSimpleName();
    private Intent before;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen3);
        this.before = getIntent();
    }

    public void goToScreen2(View view) {
        String id_row = view.getResources().getResourceName(view.getId());
        String id_event_title = "event_title" + id_row.substring(id_row.length() - 1);
        int id = getResources().getIdentifier(id_event_title, "id", this.getPackageName());
        TextView event_title = findViewById(id);

        Intent screen2 = new Intent(this, Screen2.class);
        screen2.putExtra("inputName", before.getStringExtra("inputName"));
        screen2.putExtra("guestName", before.getStringExtra("guestName"));
        screen2.putExtra("eventName", event_title.getText().toString());
        startActivity(screen2);
    }

    public void goBack(View view) {
        Intent screen2 = new Intent(this, Screen2.class);

        screen2.putExtra("guestName", before.getStringExtra("guestName"));
        screen2.putExtra("inputName", before.getStringExtra("inputName"));
        screen2.putExtra("eventName", before.getStringExtra("eventName"));

        startActivity(screen2);
    }
}
