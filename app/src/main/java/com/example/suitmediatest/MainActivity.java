package com.example.suitmediatest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToScreen2(View view) {
        TextInputEditText inputName = findViewById(R.id.inputName);
        Log.d(TAG, inputName.getText().toString());
        Intent intent = new Intent(this, Screen2.class);
        intent.putExtra("inputName", inputName.getText().toString());
        intent.putExtra("eventName", "CHOOSE EVENT");
        intent.putExtra("guestName", "CHOOSE GUEST");
        startActivity(intent);
    }

    /*
    public void isPalindrom(View view) {
        TextInputEditText inputPalindrom = findViewById(R.id.inputPalindrom);

        String forCheck = inputPalindrom.getText().toString().replaceAll("\\s+", "");
        Boolean check = true;

        int i = 0;
        int halfN = (forCheck.length() / 2) - 1;

        while ((check) && (i < halfN )){
            Character front = forCheck.charAt(i);
            Character back = forCheck.charAt(forCheck.length() - 1 - i);

            if ((front == back) || (front == Character.toUpperCase(back)) || (front == Character.toLowerCase(back))){
                i += 1;
            } else {
                check = false;
            }
        }

        if (check) {
            Toast.makeText(this, "isPalindrome", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "not palindrome", Toast.LENGTH_SHORT).show();
        }
    }*/
}
