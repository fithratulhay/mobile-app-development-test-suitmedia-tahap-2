package com.example.suitmediatest;

import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkUtils {
    private static final String LOG_TAG = NetworkUtils.class.getSimpleName();

    private static final String EVENT_BASE_URL = "https://reqres.in/api/users";

    static String getGuestInfo(String queryString) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String guestJSONString = null;

        try{
            //Build up your query URI
            Uri builtUri = Uri.parse(EVENT_BASE_URL).buildUpon()
                    .build();

            URL requestURL = new URL(builtUri.toString());

            urlConnection = (HttpURLConnection) requestURL.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            //read response using an inputStream and a StringBuffer, then convert it to a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null){
                return null;
            }

            reader = new BufferedReader(new InputStreamReader((inputStream)));
            String line;
            while((line = reader.readLine()) != null){
                buffer.append(line + "\n");
            }

            if(buffer.length() == 0){
                //stream was empty
                return null;
            }
            guestJSONString = buffer.toString();

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if(urlConnection!= null){
                urlConnection.disconnect();
            }

            if(reader != null){
                try {
                    reader.close();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            Log.d(LOG_TAG, guestJSONString);
            return guestJSONString;
        }
    }
}
