package com.example.suitmediatest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Screen2 extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView name;
    private Button Event;
    private  Button Guest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        Intent intent = getIntent();

        this.name = findViewById(R.id.name);
        this.Event = findViewById(R.id.chooseEvent);
        this.Guest = findViewById(R.id.chooseGuest);

        this.name.setText(intent.getStringExtra("inputName"));
        this.Event.setText(intent.getStringExtra("eventName"));
        this.Guest.setText(intent.getStringExtra("guestName"));
    }

    public void goToEvents(View view) {
        Intent screen3 = new Intent(this, Screen3.class);
        screen3.putExtra("inputName", this.name.getText().toString());
        screen3.putExtra("guestName", this.Guest.getText().toString());
        screen3.putExtra("eventName", this.Event.getText().toString());
        startActivity(screen3);
    }

    public void goToGuests(View view) {
        Intent screen4 = new Intent(this, Screen4.class);
        screen4.putExtra("inputName", this.name.getText().toString());
        screen4.putExtra("guestName", this.Guest.getText().toString());
        screen4.putExtra("eventName", this.Event.getText().toString());
        startActivity(screen4);
    }
}
